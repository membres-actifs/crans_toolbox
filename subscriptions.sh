#!/bin/bash

# Script mis à votre disposition
# par les gentils membres actifs du Cr@ns
# Vous pouvez l'utiliser, le redistribuer, le modifier à votre convenance.
# Des questions, des suggestions : {nounou,ca}@lists.crans.org
# Licence : WTFPL

#------------------------------------------------------------------------------

# Pour s'abonner automatiquement à tous ses dossiers mail.
# (Pratique à appeler régulièrement dans une crontab.)

# On va dans le dossier où sont rangés les mails.
cd ~/Mail

(
 # On s'abonne de toutes façons à la boîte mail principale
 echo "INBOX";
 # On fait la liste de tous les dossiers mails présents.
 # -maxdepth 1 = on ne veut pas les sous-dossiers
 # -mindepth 1 = on ne veut pas le dossier courant (.)
 # -type d = on veut les dossiers (pas les fichiers)
 # -regex … = on veut que le nom du dossier commence par un point
 # Ensuite, on trie alphabétiquement les résultats avec sort
 # (find ne sort les résultats dans aucun ordre prédéfini).
 # On élimine avec sed les 3 premiers caractères
 # de chaque résultat (qui sont "./.", le 2ème . étant le
 # premier caractère du nom du dossier, que le fichier
 # subscriptions ne doit pas contenir.)
 find . -maxdepth 1 -mindepth 1 -type d -regex "\./\..*" | sort | sed 's/^...//'
 # On envoie le tout dans le fichier ~/Mail/subscriptions,
 # qui est utilisé pour mémoriser les abonnements IMAP.
) > subscriptions
