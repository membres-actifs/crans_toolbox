# -*- coding: utf-8 -*-
#
# Copyright (C) 2006-2012 Sebastien Helleu <flashcode@flashtux.org>
# Copyright (C) 2011 Nils GÃ¶rs <weechatter@arcor.de>
# Copyright (C) 2011 ArZa <arza@arza.us>
# Copyright (C) 2013 Daniel STAN <daniel.stan@crans.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""Usage :
Dans weechat, charger le module :
 >> /python load beep.py
Si vous faîtes tourner weechat sur zamok, c'est fini, sinon, il faut
modifier la commande à appeler, pour lancer un ssh vers zamok. Je vous donne
une méthode ici pour ce faire :
 1/ Créer une clée ssh (dans mon exemple, ~/hl_irc/ssh_key) *sans passphrase*
 2/ Sur zamok, rajouter, sur une nouvelle ligne, la clé publique à votre
    .ssh/authorized_keys
 3/ Devant la ligne, préfixer par: «command="/usr/local/bin/sip_message" »
 4/ Dans weechat, modifier la commande à exécuter :
    /set plugins.var.python.beep.cmd "/usr/bin/ssh -i /home/dstan/hl_irc/ssh_key dstan@zamok.crans.org sip_message"

Vous devriez recevoir un nouveau message d'essai à chaque modification de config.
"""

SCRIPT_NAME    = 'beep'
SCRIPT_AUTHOR  = 'Daniel STAN <daniel.stan@crans.org>'
SCRIPT_VERSION = '0.9'
SCRIPT_LICENSE = 'GPL3'
SCRIPT_DESC    = 'Notify highlight/private message by a SIP message (Crans)'


import_ok = True

import subprocess

try:
    import weechat
except:
    print('This script must be run under WeeChat.')
    print('Get WeeChat now at: http://www.weechat.org/')
    import_ok = False

try:
    import os, sys
except ImportError as message:
    print('Missing package(s) for %s: %s' % (SCRIPT_NAME, message))
    import_ok = False

beep_default_options = {
    'cmd': '/usr/local/bin/sip_message',
}

beep_options = {}

def init_config():
    global beep_default_options, beep_options
    for option, default_value in beep_default_options.items():
        if not weechat.config_is_set_plugin(option):
            weechat.config_set_plugin(option, default_value)
        beep_options[option] = weechat.config_get_plugin(option)

def config_changed(data, option, value):
    init_config()
    send_message('beep.py: config reloaded !')
    return weechat.WEECHAT_RC_OK

def send_message(data):
    """Send a message through the command"""
    proc = subprocess.Popen(beep_options['cmd'], shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE)
    proc.communicate(data)

def beep_cb(data, signal, signal_data):
    send_message(signal_data)
    return weechat.WEECHAT_RC_OK

if __name__ == '__main__' and import_ok:
    if weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION,
                        SCRIPT_LICENSE, SCRIPT_DESC, '', ''):
        init_config()
        weechat.hook_config('plugins.var.python.%s.*' % SCRIPT_NAME, 'config_changed', '')
        weechat.hook_signal('weechat_highlight', 'beep_cb', 'highlight')
        weechat.hook_signal('irc_pv', 'beep_cb', 'pv')
        weechat.hook_signal('irc_dcc', 'beep_cb', 'dcc')
