#!/usr/bin/perl -w

# Script mis à votre disposition
# par les gentils membres actifs du Cr@ns
# Vous pouvez l'utiliser, le redistribuer, le modifier à votre convenance.
# Des questions, des suggestions : {nounou,ca}@lists.crans.org
# Licence : WTFPL

# Pour décoder un header mail fourni sur l'entée standard (STDIN)
# (Utilisé par le .procmailrc)

# On importe la fonction qui décode des headers
use Encode qw(decode);

# On déclare que l'encodage des print sera UTF-8
binmode(STDOUT, ":utf8");

# On lit les lignes successivement sur l'entrée standard
foreach $line ( <STDIN> ) {
    # on les décode
    my $decoded = decode('MIME-Header', $line);
    # et on les affiche
    print "$decoded";
}
