#!/bin/bash

# Pour lister de manière semi-automatique tous les serveurs.
# Ne marche que depuis un serveur crans.
# Pratique à piper dans un xargs cssh !

# Stratégie : on prend la zone .adm.crans.org et on enlève ce qui n'est pas un serveur.
# Donc :
#  * si des serveurs sont ajoutés/supprimés, rien besoin de faire
#  * si des non-serveurs sont ajoutés dans adm, il faudra corriger la regexp
#    (ou au pire, sur le cssh, ça fait une fenêtre qui meurt/ne permet pas de se loguer, on s'en remet)
#  * si un serveur NON-adm se fait ajouter, il sera pas attrappé par cette liste, c'est la merde

# IP d'un serveur qui veut bien transférer la zone adm.crans.org
NSSERVER="10.231.136.118"

# Tous les noms matchant cette regexp seront exclus
# Elle est expanded pour la lisibilité.
# Avant d'être utilisée, sont retirés :
#  * tout ce qui est entre "<" et ">" (on peut pas utiliser la fin de ligne, la commande est inlinée)
#  * les whitespaces
# (whitespace = espaces, tabulations, retours à la ligne, etc.)
REGEXP=("^(
     [^.]*\.v4					< Zone .v4. (.v6. est exclue car on regarde 'has address' et non 'has IPv6 address' >
    |(big|huge|little|tiny)brother		< Caméras >
    |vigile					< Vigile du 4J. Obsolète ? >
    |backbone|^bat[abcghijmopv]-[0-9]|minigiga	< Switches >
    |[a-z5]+-(ilo|idrac)			< Interfaces de gestion des serveurs >
    |multiprise|chu|pika			< Matériel électrique >
    |pulsar|quasar				< Onduleurs >
    |slon|nols(2)?				< Baies de disques >
    |imprimante|kvm|digicode-4j			< Autre matériel >
)\.")

# Comme promis, on nettoie la regexp
REGEXP=$(echo "${REGEXP}" | sed "s/\s<[^>]*>//g;s/\s//g" | paste -sd "")

host -l adm.crans.org 10.231.136.118 \
 | grep 'has address' \
 | awk "!/${REGEXP}/ {print \$1}"
